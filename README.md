# cc-index

## Getting started
This intends to be the central repository of all community coins by collecting information about
- their story and history
- our subjective qualification
- their objective parameters
- The intended community 

## Communitycoins.org
The Communitycoin Alliance kicked-off in may 2018 by a meeting in Lissabon between de developers of Cryptoescudo (CESC) and eGulden (EFL) and by a joint elaboration of the website communitycoins.org with a shared manifest. During the next four years the inititiative was mainly carried by the teams of eGulden, Auroracoin and Canadaecoin through weekly meetings while raiding each others communities and other teams. In 2023 Deutsche emark joined that group but the meeting frequency was reduced to allow for internal developments.

### Alliance
These teames actively contributed to the alliance and are considered community coins
- [eGulden-EFL](https://egulden.org)
- [Canadaecoin-CDN](https://canadaecoin.site/)
- [Auroracoin-AUR](https://auroracoin.is/)
- [Deutsche eMark-DEM](https://deutsche-emark.org/)
### Endorsed by the alliance
These teams are considered communitycoins but due to language or geographical/time distance had a hard time cooperating
- [Cryptoescudo-CESC](https://cryptoescudo.pt)
- [Sterlingcoin-SLG](https://sterlingcoin.org/)
- [pakcoin-PAK](https://www.pakcoin.io/)
- [Fujicoi-FJC](https://fujicoin.org/)
- [Russian Bitcoin-RUBTC](https://russian.nationalbitcoin.org/)
### Contributed 
These teams actively contributed and/or endorse community coins but the nature of their projects make them less suitable to be classified community coins. EUR because it is more about federation and NESS because it focusses on technology favoring decentralisation
- Europecoin,
- [Privateness Network-NESS](https://privateness.network/)
### In consideration 
- [Bolicoin-BOLI](https://bolis.info/) 
- [Mazacoin-MAZA](https://mazacoin.org)
- [Kobocoin-KOBO](http://kobocoin.com/)
- [Monacoin-MONA](https://monacoin.org/)
### Off the radar
- [scottcoin](https://scotcoinproject.com/) 
- [russiacoin](https://www.russiacoin.info/)
- [irishcoin](http://www.irishcoin.com/)
